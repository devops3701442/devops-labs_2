# DevOps-labs: 2.Airflow + Spark

## Описание сервиса
Сервис включает в себя настроенный файл docker-compose для развертывания Apache Airflow и ориентированный ациклический граф (DAG), состоящий из двух задач (tasks).    \
Происходит подключение Airflow к Apache Spark и через SparkSession выполняется DAG с двумя задачами:    
1) загрузка датасета ирисов;    
2) многоклассовая классификация с использованием модели логистической регрессии.        
    
В логах выводится пять строк датасета и точность (Accuracy) модели классификации.        

![data_head](./images/data_head.png)

![Accuracy](./images/Accuracy.png)

Примечания:
* В docker-compose файле вырезаны сервисы redis, airflow-worker, airflow-triggerer, airflow-cli, flower    
* USER_USERNAME и USER_PASSWORD оставлены дефолтными (airflow:airflow).При желании можно заполнить значения своими переменными.

## Локальный деплой сервиса

`git clone https://gitlab.com/devops3701442/devops-labs_2.git`

`cd devops-labs_2`

`echo -e "AIRFLOW_UID=$(id -u)" > .env`

`docker-compose up -d`

`http://localhost:8080/`

Настройка Admin Connections (Admin → Connections → + New):
* Connection Id: `spark_local`    
* Connection Type: `Spark`    
* Host: `spark://spark-master`    
* Port: `7077`    

![Add_Connection](./images/Add_Connection.png)

Запуск DAG:
![DAGs](./images/DAGs.png)

В админке Spark (`http://localhost:4040/`) отражается worker и завершенные Applications:
![Spark](./images/Spark.png)



