# История изменений

## Версия 1.0 (2023-10-23)

### Изменено
- Переписан Dockerfile: “прокинута” папка spark в образ через Dockerfile, устанавливаются библиотеки для питона apache-airflow-providers-apache-spark, а также пакеты procps и default-jre
- ml_pipeline.py перепискан под SparkSubmitOperator для передачи заданий в Spark
- Переписаны скрипты (load_data.py и log_reg_model.py) и добавлены в папку spark


